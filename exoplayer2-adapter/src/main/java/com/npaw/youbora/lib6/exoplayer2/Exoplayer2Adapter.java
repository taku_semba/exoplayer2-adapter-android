package com.npaw.youbora.lib6.exoplayer2;

import android.support.annotation.Nullable;

import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.ExoPlayerLibraryInfo;
import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.MappingTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.upstream.BandwidthMeter;

import com.npaw.youbora.lib6.Timer;
import com.npaw.youbora.lib6.YouboraLog;
import com.npaw.youbora.lib6.YouboraUtil;
import com.npaw.youbora.lib6.adapter.PlayerAdapter;
import com.npaw.youbora.lib6.plugin.Plugin;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

public class Exoplayer2Adapter extends PlayerAdapter<ExoPlayer> implements Player.EventListener {

    protected PlayerQualityProvider qualityProvider;
    protected ExoplayerWindowChangedListener windowChangedListener;
    protected BandwidthMeter bandwidthMeter;
    protected int currentWindowIndex;

    private double playheadZero;

    private double userReportedBitrate;

    private double lastPosition;

    private Timer joinTimer;

    private CustomEventLogger customEventLogger;
    private Boolean customEventLoggerEnabled;

    public Exoplayer2Adapter(ExoPlayer player) {
        super(player);

        registerListeners();
        playheadZero = 0.1;
        userReportedBitrate = -1;
        lastPosition = 0;
    }

    @Override
    public void registerListeners() {
        super.registerListeners();

        currentWindowIndex = 0;

        buildQualityProvider(false);
        getPlayer().addListener(this);

        joinTimer = new Timer(new Timer.TimerEventListener() {
            @Override
            public void onTimerEvent(long delta) {
                if(getPlayer() != null && getPlayhead() != null){
                    if(getPlayhead() > playheadZero + 0.1){
                        if(!getFlags().isStarted()){
                            fireStart();
                        }
                        fireJoin();
                        if(getFlags().isJoined()){
                            YouboraLog.debug("Detected join time at playhead: " + String.valueOf(getPlayhead()));
                            joinTimer.stop();
                        }
                    }
                }
            }
        },100);
    }

    @Override
    public void unregisterListeners() {
        getPlayer().removeListener(this);

        super.unregisterListeners();
    }
    
    // Get methods
    @Override
    public String getVersion() {
        return BuildConfig.VERSION_NAME + "-ExoPlayer2";
    }

    @Override
    public String getPlayerName() {
        return "ExoPlayer2";
    }

    @Override
    public String getPlayerVersion() {

        /*
         * If we just try to access ExoPlayerLibraryInfo.VERSION we will always report the version
         * of Exoplayer we're compiling the plugin with.
         *
         * As a workaround, we use reflection in order to get the class at runtime and access its
         * VERSION field.
         *
         * If this fails, we report "unknown".
         */
        StringBuilder versionBuilder = new StringBuilder("ExoPlayer2-");

        try {
            Field versionField = ExoPlayerLibraryInfo.class.getDeclaredField("VERSION");
            versionBuilder.append((String) versionField.get(null));
        } catch (Exception e) {
            versionBuilder.append("unknown");
        }

        return versionBuilder.toString();
    }

    @Override
    public Double getPlayhead() {
        //We don't use getPlayer().getContentPosition() because we need to keep playhead
        //when ads started, not when they are going to end
        double position = (double) getPlayer().getCurrentPosition() / 1000;

        if(getPlugin() != null
                && getPlugin().getIsLive() != null
                && getPlugin().getIsLive()) {
            return -1.0;
        }

        if(getPlayer().isPlayingAd()){
            return lastPosition;
        }
        //Just saving it for later use
        lastPosition = position;
        return position;
    }

    @Override
    public Double getDuration() {
        Double duration;
        long playerDuration = getPlayer().getDuration();
        if (playerDuration == C.TIME_UNSET) {
            duration = super.getDuration();
        } else {
            duration = (double) (playerDuration / 1000); // msec -> sec
        }
        return duration;
    }

    @Override
    public Long getThroughput() {
        Long throughput = super.getThroughput();

         if (bandwidthMeter != null) {
            // When playing non-segmented video transport streams (such as an mp4 file)
            // the throughput info is not reliable. Bitrate for those files is null as well,
            // so we won't extract the throughput if the bitrate is zero.
            if (qualityProvider != null){
                Long bitrate = qualityProvider.getBitrate();
                if (bitrate != null && bitrate > 0) {
                    throughput = bandwidthMeter.getBitrateEstimate();
                }
            }
        }
        return throughput;
    }

    @Override
    public Long getBitrate() {
        Long bitrate;

        if (customEventLoggerEnabled)
            bitrate = Long.valueOf(customEventLogger.getBitrate());
        else if (qualityProvider != null)
            bitrate = qualityProvider.getBitrate();
        else
            bitrate = super.getBitrate();

        return bitrate;
    }

    @Override
    public String getRendition() {
        String rendition;
        if (qualityProvider == null) {
            rendition = super.getRendition();
        } else {
            rendition = qualityProvider.getRendition();
        }
        return rendition;
    }

    @Override
    public Double getFramesPerSecond() {
        Double fps;
        if (qualityProvider == null) {
            fps = super.getFramesPerSecond();
        } else {
            fps = qualityProvider.getFramerate();
        }
        return fps;
    }

    /**
     * Sets a {@link BandwidthMeter} in order to allow the Adapter to get the Throughput ({@link BandwidthMeter#getBitrateEstimate()}).
     * @param bandwidthMeter Bandwidth meter to set
     */
    public void setBandwidthMeter(BandwidthMeter bandwidthMeter) {
        this.bandwidthMeter = bandwidthMeter;
    }

    /**
     * Sets an <b>optional</b> {@link PlayerQualityProvider}. In case you're <b>NOT</b> using the
     * SimpleExoPlayer, you can implement and set a PlayerQualityProvider to the Adapter in order
     * to extract the bitrate and rendition.
     * @param qualityProvider {@link PlayerQualityProvider} to set
     */
    public void setQualityProvider(PlayerQualityProvider qualityProvider) {
        this.qualityProvider = qualityProvider;
    }

    /**
     * Sets a {@link ExoplayerWindowChangedListener} to this Adapter. This callback will be invoked
     * when new content is about to start.
     *
     * The main tasks of this listener is to inform the resource and title of the media about
     * to be played calling {@link com.npaw.youbora.lib6.plugin.Options#setContentResource(String)}
     * and {@link com.npaw.youbora.lib6.plugin.Options#setContentTitle(String)}.
     *
     * This can also be achieved by adding a
     * {@link com.npaw.youbora.lib6.plugin.Plugin#addOnWillSendStartListener(Plugin.WillSendRequestListener)}.
     *
     * @param windowChangedListener the listener to set.
     */
    public void setWindowChangedListener(ExoplayerWindowChangedListener windowChangedListener) {
        this.windowChangedListener = windowChangedListener;
    }

    protected void buildQualityProvider(boolean force) {
        /*
         * Only can extract this info from a SimpleExoPlayer. If it is one, create the Provider.
         * Otherwise (ExoPlayer) return a default implementation.
         */
        if (force || qualityProvider == null) {
            if (getPlayer() instanceof SimpleExoPlayer) {
                qualityProvider = new PlayerQualityProvider<SimpleExoPlayer>((SimpleExoPlayer) player) {
                    @Override
                    public Long getBitrate() {
                        Long bitrate;
                        Format format = this.player.getVideoFormat();
                        if (customEventLoggerEnabled)
                            bitrate = Long.valueOf(customEventLogger.getBitrate());
                        else if (format != null && format.bitrate != Format.NO_VALUE)
                            bitrate = (long) format.bitrate;
                        else
                            bitrate = (long) userReportedBitrate;

                        return bitrate;
                    }

                    @Override
                    public String getRendition() {
                        Format format = this.player.getVideoFormat();

                        int bitrate = 0;
                        int width = 0;
                        int height = 0;

                        if (format != null) {
                            bitrate = getBitrate().intValue();
                            width = format.width;
                            height = format.height;
                        }

                        if ((width <= 0 || height <= 0) && bitrate <= 0) {
                            return super.getRendition();
                        } else {
                            return YouboraUtil.buildRenditionString(width, height, bitrate);
                        }
                    }

                    @Override
                    public Double getFramerate() {
                        Double fps;
                        Format format = this.player.getVideoFormat();
                        if (format != null && format.frameRate != Format.NO_VALUE) {
                            fps = (double) format.frameRate;
                        } else {
                            fps = super.getFramerate();
                        }
                        return fps;
                    }
                };
            } else {
                qualityProvider = new PlayerQualityProvider<>(player);
            }
        }
    }

    @Override
    public Boolean getIsLive() {
        return getPlayer().isCurrentWindowDynamic();
    }

    // Player.EventListener interface methods

    @Override
    public void onTimelineChanged(Timeline timeline, Object o, int i) {

    }

    @Override
    public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {
        // Not used
    }

    @Override
    public void onLoadingChanged(boolean isLoading) {
        // Not used
    }

    @Override
    public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
        String debugStr;

        switch (playbackState) {
            case Player.STATE_BUFFERING:
                debugStr = "onPlayerStateChanged: STATE_BUFFERING";
                stateChangedBuffering();
                break;
            case Player.STATE_ENDED:
                debugStr = "onPlayerStateChanged: STATE_ENDED";
                stateChangedEnded();
                break;
            case Player.STATE_IDLE:
                debugStr = "onPlayerStateChanged: STATE_IDLE";
                stateChangedIdle();
                break;
            case Player.STATE_READY:
                debugStr = "onPlayerStateChanged: STATE_READY";
                stateChangedReady();
                break;
            default:
                debugStr = "onPlayerStateChanged: unknown state - " + Integer.toString(playbackState);
                break;
        }

        stateChangedPlayWhenReady(playWhenReady);
        YouboraLog.debug(debugStr + ", playWhenReady " + playWhenReady);
    }

    @Override
    public void onRepeatModeChanged(int repeatMode) {

    }

    @Override
    public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) {

    }

    @Override
    public void onPlayerError(ExoPlaybackException error) {

        String classError = error.getCause().getClass().getName();
        // The error message sometimes is very verbose, and has the resource in it, so we cannot
        // use it as the error message. We send it as errorMetadata. Other times though, it is null.

        // We use the cause's class name as the error code and description.
        String errorMessage = error.getMessage();
        if (errorMessage != null && errorMessage.length() != 0) {
            fireFatalError(classError, classError, errorMessage);
        } else {
            fireFatalError(classError, classError, null);
        }
        fireStop();
        YouboraLog.debug("onPlayerError: " + error.toString());
    }

    @Override
    public void onPositionDiscontinuity(int reason) {
        YouboraLog.debug("onPositionDiscontinuity");

        //In case there's something to do with the reason
        switch (reason){
            case Player.DISCONTINUITY_REASON_PERIOD_TRANSITION:
                break;
            case Player.DISCONTINUITY_REASON_SEEK:
                break;
            case Player.DISCONTINUITY_REASON_SEEK_ADJUSTMENT:
                break;
            case Player.DISCONTINUITY_REASON_INTERNAL:
                break;
        }

        int windowIndex = getPlayer().getCurrentWindowIndex();
        playheadZero = getPlayhead();
        /*
         * If the window index hasn't changed, this is just a seek, otherwise it's a resource
         * change, like in a playlist.
         */
        if (windowIndex != currentWindowIndex) {
            // Playlist item change
            fireStop();

            fireStart();
            if (getPlayer().getPlaybackState() != Player.STATE_BUFFERING) {
                //fireJoin();
                joinTimer.start();
            }
        } else {
            fireSeekBegin(true);
        }
    }

    @Override
    public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {

    }

    @Override
    public void onSeekProcessed() {

    }

    // Intermediate methods. This allows the user of the Adapter to re define behaviour by
    // inheritance in a granular way.
    protected void stateChangedBuffering() {
        if (!getFlags().isStarted()) {
            fireStart();
        } else {
            if(!getFlags().isPaused()){
                fireBufferBegin();
            }
        }
    }

    protected void stateChangedEnded() {
        fireStop();
    }

    protected void stateChangedIdle(){
        fireStop();
    }

    protected void stateChangedReady() {
        // Maybe we've missed the preparing event, this happens in a replay
        if (!getFlags().isStarted()) {
            fireStart();
        }
        if(getFlags().isSeeking()){
            playheadZero = getPlayhead();
        }
        if (getFlags().isJoined()) {
            fireSeekEnd();
            fireBufferEnd();
        } else {
            //fireJoin();
            if(getPlugin() != null
                    && getPlugin().getIsLive() != null
                    && getPlugin().getIsLive()) {
                fireJoin();
            }
        }
    }

    protected void stateChangedPlayWhenReady(boolean playWhenReady) {
        if (playWhenReady) {
            fireResume();
        } else {
            firePause();
        }
    }

    /*protected void listenForJoinTime(){

        if(joinTimer != null && !getFlags().isJoined() && !joinTimer.isRunning()){
            joinTimer.stop();
        }
    }*/

    // Overridden Adapter methods
    @Override
    public void fireStart() {
        // Save the windowIndex, aka. the playlist index
        currentWindowIndex = getPlayer().getCurrentWindowIndex();

        // Every time a play is about to start, call the delegate to give the client code
        // the chance to update the resource and title since the plugin is not capable
        // of getting those
        if (windowChangedListener != null){
            windowChangedListener.onExoplayerWindowChanged(this, currentWindowIndex);
        }
        reset();
        playheadZero = getPlayhead() == 0 ? 0.1 : getPlayhead();
        joinTimer.start();
        super.fireStart();
    }

    @Override
    public void fireStop(Map<String, String> params) {
        if (params == null) {
            params = new HashMap<>();
        }

        // Force playhead to -1 to avoid Exoplayer2 limitation when in playlist
        params.put("playhead", "-1");
        //Reset joinTIme

        super.fireStop(params);
    }

    @Override
    public void fireError(String msg, String code, String errorMetadata, Exception exception){
        if(exception instanceof ExoPlaybackException){
            return;
        }
        super.fireError(msg,code,errorMetadata);
    }

    @Override
    public void fireFatalError(String msg, String code, String errorMetadata, Exception exception){
        if(exception instanceof ExoPlaybackException){
            return;
        }
        super.fireFatalError(msg,code,errorMetadata);
    }

    public void setBitrate(double userReportedBitrate){
        this.userReportedBitrate = userReportedBitrate;
    }

    private void reset(){
        playheadZero = 0.1;
        userReportedBitrate = -1;
        lastPosition = 0;
    }

    /**
     * The main tasks of this listener is to inform the resource and title of the media about
     * to be played calling {@link com.npaw.youbora.lib6.plugin.Options#setContentResource(String)}
     * and {@link com.npaw.youbora.lib6.plugin.Options#setContentTitle(String)}.
     * @see #onExoplayerWindowChanged(Exoplayer2Adapter, int)
     */
    public interface ExoplayerWindowChangedListener {
        /**
         * This method will be called by the plugin whenever a new "Window", aka. media file
         * is about to start playing. Since the plugin is not able to get the resource and title,
         * those must be set in this callback.
         *
         * @param adapter The PlayerAdapter instance performing this callback
         * @param newWindowIndex The index (starting at 0) of the newly loaded media
         */
        void onExoplayerWindowChanged(Exoplayer2Adapter adapter, int newWindowIndex);
    }



    /**
     * Class to extract the bitrate and rendition from the player.
     * @see #setQualityProvider(PlayerQualityProvider)
     *
     * @param <T> Type of the Player, usually ExoPlayer
     */
    public static class PlayerQualityProvider<T> {

        T player;

        public PlayerQualityProvider(T player){
            this.player = player;
        }

        public Long getBitrate() {
            return null;
        }

        public String getRendition() {
            return null;
        }

        public Double getFramerate() {
            return null;
        }
    }

    public void setCustomEventLogger(@Nullable MappingTrackSelector trackSelector) {
        if (getPlayer() instanceof  SimpleExoPlayer) {
            SimpleExoPlayer player = (SimpleExoPlayer) getPlayer();
            customEventLogger = new CustomEventLogger(trackSelector);
            player.addAnalyticsListener(customEventLogger);
            customEventLoggerEnabled = true;
        }
    }
}
