package com.npaw.youbora.lib6.exoplayer2;

import android.support.annotation.Nullable;

import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.source.MediaSourceEventListener;
import com.google.android.exoplayer2.trackselection.MappingTrackSelector;
import com.google.android.exoplayer2.util.EventLogger;

public class CustomEventLogger extends EventLogger {
    private Integer bitrate;

    public CustomEventLogger(@Nullable MappingTrackSelector trackSelector) {
        super(trackSelector);
    }

    @Override
    public void onLoadCompleted(EventTime eventTime,
                                MediaSourceEventListener.LoadEventInfo loadEventInfo,
                                MediaSourceEventListener.MediaLoadData mediaLoadData) {
        super.onLoadCompleted(eventTime, loadEventInfo, mediaLoadData);
        if (mediaLoadData.trackFormat != null)
            if (mediaLoadData.trackType == C.TRACK_TYPE_DEFAULT ||
                    mediaLoadData.trackType == C.TRACK_TYPE_VIDEO)
                bitrate = mediaLoadData.trackFormat.bitrate;
        // Do nothing
    }

    public Integer getBitrate() {
        return bitrate;
    }
}