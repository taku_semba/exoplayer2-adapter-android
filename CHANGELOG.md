## [6.0.15] - 2018-
### Added
- Now the CustomEventLogger class is included within the adapter

## [6.0.14] - 2018-07-20
### Fixed
 - Fix rendition bitrate for HLS
 
## [6.0.13] - 2018-07-10
### Fixed
 - Fix join time for live videos

## [6.0.12] - 2018-06-27
### Added
 - Support for ExoPlayer 2.8
### Fixed
 - Some cases join time could not be fired

## [6.0.11] - 2018-05-22
### Fixed
 - Join time in case of IMA extension

## [6.0.10] - 2018-05-14
### Added
 - Use STATE_IDLE or STATE_ENDED to stop playback

## [6.0.9] - 2018-03-23
### Added
 - Support for ExoPlayer 2.7
 - Compiled with YouboraLib 6.1.4

## [6.0.8] - 2018-02-19
### Added
 - Compiled with YouboraLib 6.1.1

## [6.0.7] - 2018-02-13
### Added
 - Compiled with YouboraLib 6.1.0

## [6.0.6] - 2018-01-10
### Added
 - Support for ExoPlayer 2.6
 
## [6.0.5] - 2018-01-10
### Added
 - Last version supporting ExoPlayer 2.5
 - Offline mode to example
 
## [6.0.4] - 2017-12-22
### Added
 - Last version supporting ExoPlayer 2.4
 - Update to YouboraLib 6.0.9
 
## [6.0.3] - 2017-12-18
### Added
 - Default playhead in case of live
 
## [6.0.2] - 2017-12-15
### Added
 - Null check on playhead
 - Automatic joinTime even when playback doesn't start from begining
 
## [6.0.1] - 2017-10-06
### Added
 - Update ExoPlayer to 2.4.3
 
## [6.0.0] - 2017-07-18
### Added
 - First release
 